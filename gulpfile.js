var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var buffer = require('vinyl-buffer');
var imagemin = require('gulp-imagemin');
var merge = require('merge-stream');
var spritesmith = require('gulp.spritesmith');
var replace = require('gulp-string-replace');

gulp.task('default', function(){
    watch('./src/**/*.scss', function(){
        gulp.start('build');
    });
});

gulp.task('build',function(){
    return gulp.src('./src/**/*.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('./dist'));
});

gulp.task('sprite', function(){
    var spriteData = gulp.src('./img/sprites/*.png').pipe(spritesmith({
        imgName: 'sprites.png',
        cssName: '_sprites.scss'
    }));

    var imgStream = spriteData.img
        .pipe(buffer())
        .pipe(imagemin())
        .pipe(gulp.dest('./dist/'));

    var cssStream = spriteData.css
        .pipe(replace(/sprites.png/g, function(){
            return '%%sprites%%'; // replace sprites.png with reddit image name convention
        }))
        .pipe(gulp.dest('./src/misc/'));

    return merge(imgStream, cssStream);
});
