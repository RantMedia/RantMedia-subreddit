### Quick installation instructions ###

* Download and install [NodeJS](https://nodejs.org), [NPM](https://www.npmjs.com/), and [gulp](http://gulpjs.com/)
* clone the repository:
    * ```git clone https://seanidzenga@bitbucket.org/seanidzenga/rantmedia-subreddit.git```
    * ```git clone git@bitbucket.org:seanidzenga/rantmedia-subreddit.git```
* open console/terminal/command-prompt and cd into the folder where you cloned this repo
* run ```npm install``` to install all required dependencies
* run ```gulp``` to begin the background build process

### What's this all about? ###

A custom CSS theme built using sass for the RantMedia subreddit. This may be a good jumping point for building a generic subreddit styling framework!

### About the build process ###

Using the gulp build process we're able to actively watch all of our sass files and continuously build our compiled CSS for the RantMedia subreddit.

Any change will prompt a compilation, this means adding and deleting files as well. The build process does not include spritesheets as rebuilding the spritesheet on every save would be inefficient.

### What's all this about spritesheets? ###

It's a common web-dev practice to compile many small reusable images into one image and using some CSS display the appropriate region of the image. This saves on load time as most browsers limit the number of concurrent downloads, so rather than downloading 30 or so tiny images at the same time and holding up the load process we place them all in one image and send it down the tube all at once.

Doing this manually however can be a nightmare as you'd have to place the images together, minding how close they are together and manually entering the x/y coordinates for later use in your CSS. Fortunately, there's a wonderful package called [gulp.spritesmith](https://github.com/twolfson/gulp.spritesmith) which takes care of all the heavy lifting for us.

Simply add your sprite to ```./img/sprites/``` and take note of the image's filename, then in your command-line navigate to the project directory and run ```gulp sprite```

Now when you want to add the sprite to a css class, in sass you would write:
```
// given an image file named foo.png

.foobar {
    @include sprite($foo);
}
```

### Get SASSy! ###

[SASS](http://sass-lang.com) (short for Syntactically Awesome StyleSheets) is a CSS pre-processor language. Maintaining CSS quickly becomes a readability nightmare as a stylesheet grows, SASS adds some extra functionality to relieve the burden of maintenance from the developer, a few of these include:
- **Partials** - allowing you to break your sass files up into logical fragments and including them in a master file.
- **Nesting** - instead of tediously rewriting rules for increasing orders of specificity you can nest selectors within each other and SASS will generate the proper rules automatically.
- **Variables** - this greatly relieves the developer from having to hunt down rules that share identical values by allowing you to substitute a variable name and modify its values

For more information on SASS's basics, go [here](http://sass-lang.com/guide)

### Put a comb through it ###

To keep our conventions consistent I've employed the use of [CSSComb](https://atom.io/packages/atom-css-comb) as a package for the [Atom text editor](https://atom.io/). Under ```./config``` you'll find a ```.csscomb.json``` file which can be used with CSSComb to ensure each scss file is kept consistent with the rest of the project.

In the future this may be replaced with a gulp task, which will hopefully eliminate any cross-platform nonsensery.

### Project structure ###

```
.
+-- config
|   +-- .csscomb.json
+-- dist
|   +-- <compiled files here>
+-- img
|   +-- sprites
|   |   +-- <individual sprites here>
|   +-- <other images used in the subreddit here>
+-- src
|   +-- feed
|   |   +-- _flairs.scss
|   |   +-- _links.scss
|   +-- header
|   |   +-- _banner.scss
|   |   +-- _mySubreddits.scss
|   |   +-- _tabMenu.scss
|   |   +-- _variables.scss
|   +-- misc
|   |   +-- _animations.scss
|   |   +-- _shorthand.scss
|   |   +-- _sprites.scss
|   +-- sidebar
|   |   +-- _sidebar.scss
|   +-- rantmedia.scss
+-- gulpfile.js
+-- package.json
+-- README.md
```

**dist** short for 'distribution' is where any compiled and ready for production files will be generated. This folder will generally be empty until the first run of ```gulp```

**src/\*** these folders are logically fragmented pieces of the stylesheet, with ```rantmedia.scss``` being our master file. All scss files should be included in ```rantmedia.scss```

**img** contains images used in the subreddit and a sprites folder. These images aren't included in ```dist``` yet but that is very likely to change soon.

### Who do I talk to? ###

* [Sean Idzenga](mailto:sean.idzenga@gmail.com)
